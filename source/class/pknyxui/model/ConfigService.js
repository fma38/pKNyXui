qx.Class.define("pknyxui.model.ConfigService",
{
    type: "singleton",

    extend: qx.core.Object,

    properties:
    {
        config: {
            nullable: true
        }
    },

    events:
    {
        "loaded": "qx.event.type.Event"
    },

    construct: function() {
        this._rest.addListener("loadSuccess", this._onLoaded, this);
        this._rest.addListener("loadError", function(e) {
            this.debug("ERROR::Failed to load config!")
        }, this);
    },

    members:
    {
        _rest: new qx.io.rest.Resource({
            load: {
                method: "GET",
                url: "/config"
            }
        }),

        // Called when objects property changes
        _onLoaded: function(e) {
            this.debug("TRACE::_onLoaded()");
            var data = e.getData();
            if (data != null) {
                this.setConfig(data);

                this.fireEvent("loaded");
            }
        },

        load: function() {
            this.debug("TRACE::load()");
            this._rest.load();
        }
    }
});
