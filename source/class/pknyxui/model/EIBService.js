qx.Class.define("pknyxui.model.EIBService",
{
    type: "singleton",

    extend: qx.core.Object,

    properties:
    {
    },

    events:
    {
    },

    construct: function() {
        this.base(arguments);

//         this._rest.configureRequest(function(req, action, params, data) {
//             if (action == "readPoll") {
//                 req.setRequestData("bla");
//             }
//         });
        this._rest.addListener("readSuccess", this._onReadSuccess, this);
        this._rest.addListener("readPollSuccess", this._onReadPollSuccess, this);
        this._rest.addListener("error", function(e) {
            var action = e.getAction();
            this.debug("ERROR::action", action, "failed");
            alert("Connection lost");
        }, this);
    },

    members:
    {
        _listeners: {},
        _ts: 0,
        _rest: new qx.io.rest.Resource({
            read: {
                method: "GET",
                url: "/eib/{ts}"
            },
            readPoll: {
                method: "GET",
                url: "/eib/{ts}"
            },
            write: {
                method: "PUT",
                url: "/eib/{id}/{knx}"
            }
        }),

        _onReadSuccess: function(e) {
//             this.debug("_onReadSuccess():", e.getRequest().getAllResponseHeaders());
            var data = e.getData();
            this._update(data);
        },

        _onReadPollSuccess: function(e) {
//             this.debug("_onReadPollSuccess():", e.getRequest().getAllResponseHeaders());
            var data = e.getData();
            this._update(data);

            // Emulate long poll
            setTimeout(this.readPoll(this._ts), 1000);  // ensure some delay between 2 polls
        },

        _update: function(data) {
            this._ts = data['ts'];
            this.debug("_update(): new ts=", this._ts);
            var eib = data['eib'];
            eib.forEach(function(item, index, array) {
                this.debug("_update() eib:", item);
                var id = item['id'];
                var knx = item['knx'];
                var value = item['value'];
                if (this._listeners.hasOwnProperty(id)) {
                    if (this._listeners[id].hasOwnProperty(knx)) {
                        this._listeners[id][knx].forEach(function(item2, index2, array2) {
                            try {
                                item2.update(id, knx, value);
                            }
                            catch (e) {
                                this.debug("ERROR::_update()", e);
                            }
                        }, this);
                    }
                }
            }, this);
        },

        read: function() {
            this._rest.read({'ts': 0});
        },

        readPoll: function() {
            this._rest.readPoll({'ts': this._ts});
        },

        write: function(id, knx, value) {
            this._rest.write({'id': id, 'knx': knx}, {'value': value});
        },

        registerListener: function(id, knx, obj) {
            knx.forEach(function(item, index, array) {
                try {
                    if (!this._listeners[id][item].contains(obj)) {
                        this._listeners[id][item].push(obj);
                        this.debug("registerListener(): listener", obj, "registered to id=", id, ", knx=", item);
                    }
                    else {
                        this.debug("WARNING::registerListener() 1: listener", obj, " already registered id=", id, ", knx=", item);
                    }
                }
                catch (e) {
                    if (e instanceof TypeError) {
                        try {
                            this._listeners[id][item] = new qx.data.Array();
                            this._listeners[id][item].push(obj);
                            this.debug("registerListener(): listener", obj, " registered id=", id, ", knx=", item);
                        }
                        catch (f) {
                            if (f instanceof TypeError) {
                                this._listeners[id] = {}
                                this._listeners[id][item] = new qx.data.Array();
                                this._listeners[id][item].push(obj);
                                this.debug("registerListener(): listener", obj, " registered id=", id, ", knx=", item);
                            }
                            else {
                                this.debug("ERROR::registerListener():", f);
                            }
                        }
                    }
                    else {
                        this.debug("ERROR::registerListener():", e);
                    }
                }

//                 this.debug("registerListener():", this._listeners[id][item].length, "listeners for id=", id, ", knx=", item, ":");
//                 this._listeners[id][item].forEach(function(item2, index2, array2) {
//                     this.debug(item2);
//                 }, this);
            }, this);
        },

        run: function() {
//             this._rest.longPoll("readNew");
            this.readPoll(this._ts);
        }
    }
});
