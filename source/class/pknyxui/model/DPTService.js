qx.Class.define("pknyxui.model.DPTService",
{
    type: "singleton",

    extend: qx.core.Object,

    properties:
    {
    },

    events:
    {
    },

    members:
    {
        _DPT: {
            "1.xxx": {0: 0, 1: 1},  // DPT_Generic (1.xxx)
            "1.001": {0: "Off", 1: "On", "Off": 0, "On": 1}  // DPT_Switch (1.001)
//     DPT_Bool = DPT("1.002", "Boolean", (False, True))
//     DPT_Enable = DPT("1.003", "Enable", ("Disable", "Enable"))
//     DPT_Ramp = DPT("1.004", "Ramp", ("No ramp", "Ramp"))
//     DPT_Alarm = DPT("1.005", "Alarm", ("No alarm", "Alarm"))
//     DPT_BinaryValue = DPT("1.006", "Binary value", ("Low", "High"))
//     DPT_Step = DPT("1.007", "Step", ("Decrease", "Increase"))
//     DPT_UpDown = DPT("1.008", "Up/Down", ("Up", "Down"))
//     DPT_OpenClose = DPT("1.009", "Open/Close", ("Open", "Close"))
//     DPT_Start = DPT("1.010", "Start", ("Stop", "Start"))
//     DPT_State = DPT("1.011", "State", ("Inactive", "Active"))
//     DPT_Invert = DPT("1.012", "Invert", ("Not inverted", "Inverted"))
//     DPT_DimSendStyle = DPT("1.013", "Dimmer send-style", ("Start/stop", "Cyclically"))
//     DPT_InputSource = DPT("1.014", "Input source", ("Fixed", "Calculated"))
//     DPT_Reset = DPT("1.015", "Reset", ("No action", "Reset"))
//     DPT_Ack = DPT("1.016", "Acknowledge", ("No action", "Acknowledge"))
//     DPT_Trigger = DPT("1.017", "Trigger", ("Trigger", "Trigger"))
//     DPT_Occupancy = DPT("1.018", "Occupancy", ("Not occupied", "Occupied"))
//     DPT_Window_Door = DPT("1.019", "Window/Door", ("Closed", "Open"))
//     DPT_LogicalFunction = DPT("1.021", "Logical function", ("OR", "AND"))
//     DPT_Scene_AB = DPT("1.022", "Scene A/B", ("Scene A", "Scene B"))
//     DPT_ShutterBlinds_Mode = DPT("1.023", "Shutter/Blinds mode", ("Only move Up/Down", "Move Up/Down + StepStop"))
        },

        convert: function(dpt, value) {
//             this.debug("convert(): dpt=", dpt);
            if (this._DPT[dpt].hasOwnProperty(value)) {
                return this._DPT[dpt][value];
            }
        }
    }
});
