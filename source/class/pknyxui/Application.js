qx.Class.define("pknyxui.Application",
{
    extend : qx.application.Mobile,

    members :
    {
        _menuPage: null,
        _defaultWidgetsPage: null,
        _manager: null,

        main : function() {
            this.base(arguments);

            // Enable logging in debug variant
            if (qx.core.Environment.get("qx.debug")) {

                // support native logging capabilities, e.g. Firebug for Firefox
                qx.log.appender.Native;
                // support additional cross-browser console.
                // Trigger a "longtap" event on the navigation bar for opening it.
                qx.log.appender.Console;
            }

            // Load config
            var configService = pknyxui.model.ConfigService.getInstance();
            configService.addListener("loaded", this._onConfigLoaded, this);
            configService.load();

            this._manager = new qx.ui.mobile.page.Manager();
        },

        _onConfigLoaded: function(e) {
            this.debug("TRACE::_onConfigLoaded()");

            // Parse the config and create pages/widgets
            var config = pknyxui.model.ConfigService.getInstance().getConfig();
            for (var key in config) {
                switch (key) {
                    case "pages":
                        this._createPages("Menu principal", "", config["pages"], 0);
                        break;

                    case "widgets":
                        this.debug("WARNING::_onConfigLoaded(): widgets not yet implemented at top level");
                        break;

                    default:
                        this.debug("ERROR::_onConfigLoaded(): unknow key", key);
                        break;
                }
            }

            if (qx.core.Environment.get("device.type") == "tablet" || qx.core.Environment.get("device.type") == "desktop") {
                this.getRouting().onGet("/.*", this._show, this._menuPage);
                this.getRouting().onGet("/", this._show, this._defaultWidgetsPage);
            }
            else {
                this.getRouting().onGet("/", this._show, this._menuPage);
            }

            this.getRouting().init();

            // Start eib polling
            pknyxui.model.EIBService.getInstance().run();
        },

        _createPages: function(title, path, pages, level) {
            this.debug("_createPages(): level=", level);
            var links = new qx.data.Array();
            pages.forEach(function(item, index, array) {

                // Populate links list
                links.push({'title': item['title'], 'subtitle': item['subtitle'], 'path': item['path']});

                // Create matching navigation page
                if (item.hasOwnProperty('pages')) {
                    this._createPages(item['title'], item['path'], item['pages'], level+1);
                }
                else if (item.hasOwnProperty('widgets')) {

                    // Create a widgets page
                    var widgetsPage = new pknyxui.page.WidgetsPage(item['title'], item['widgets']);
                    if (level > 0) {
                        widgetsPage.setShowBackButton(true);
                        widgetsPage.setShowBackButtonOnTablet(true);
                    }
                    if (this._defaultWidgetsPage == null) {
                        this._defaultWidgetsPage = widgetsPage;
                    }
                    this._manager.addDetail(widgetsPage);
                    this.getRouting().onGet("/"+item['path'], this._show, widgetsPage);

                    this.debug("_createPages(): added widgets page", item['title']);
                }
                else {
                    this.debug("WARNING::_createPages(): page list item should either has widgets or pages property");
                }
            }, this);

            // Create the links page
            var linksPage = new pknyxui.page.LinksPage(links);
            linksPage.setTitle(title);
            if (level == 0) {
                this._menuPage = linksPage;
                this._manager.addMaster(linksPage);
            }
            else {
                linksPage.setAltCss();
                this._manager.addDetail(linksPage);
                this.getRouting().onGet("/"+path, this._show, linksPage);
            }
        },

        _show : function(data) {
            this.show(data.customData);
        }
    }
});
