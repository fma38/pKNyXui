/*
 * A simple display widget (floats only, at the moment)
 *
 * options:
 * - 'decimals'
 * - 'prefix'
 * - 'postfix'
 *
 * input objects:
 * - 'value': object used by the widget to show the value (DPT 9.xxx)
 *
 * output objects:
 *
 * TODO: give dpt instead of prefix, decimals (or allow both)
 * TODO: make a base display, and some specialized displays (string, int, float...)
 */
qx.Class.define("pknyxui.widget.DisplayMessageWidget",
{
    extend: pknyxui.widget.BaseWidget,

    members:{
        _numberFormat: null,

        _init: function() {
            this.debug("TRACE::_init()");
//             this.debug("DEBUG::_init(): options=", this._options);
        },

        _registerCallbacks: function() {
            this.debug("TRACE::_registerCallbacks()");

            pknyxui.model.EIBService.getInstance().registerListener(this._id, ["message"], this);
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");

            // Display
            this._labelButton = new qx.ui.mobile.form.Button("");
            this._labelButton.setActivatable(false);
//             this._labelButton.addCssClass("");
            this.add(this._labelButton);
        },

        update: function(id, knx, value) {
            this.debug("update(): id=", id, ", knx=", knx, ", value=", value);
            if (value < this._options['message'].length) {
                this._labelButton.setValue(this._options['message'][value]);
            }
            else {
                this._labelButton.setValue("Wrong message!");
            }
        }
    }
});
