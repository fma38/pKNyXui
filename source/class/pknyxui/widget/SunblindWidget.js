/*
 * A sunblind widget, providing blind/louvre drive and position feedback
 *
 * input objects:
 * - 'blind_position': object used by the widget to show the blind position (DPT_Scaling 5.001)
 * - 'louvre_position': object used by the widget to show the louvre position (DPT_Scaling 5.001)
 *
 * output objects:
 * - 'blind_updown': object driven by the widget to move blind up/down (DPT_UpDown 1.008)
 * - 'louvre_step': object driven by the widget to move louvre open/close (DPT_Step 1.007)
 * - 'louvre_moveto': object driven by the widget to set the louvre position (DPT_Scaling 5.001)
 *
 */
qx.Class.define("pknyxui.widget.SunblindWidget",
{
    extend: pknyxui.widget.BaseWidget,

    members:
    {
        _louvrePopup: null,
        _blindPopup: null,

        _registerCallbacks: function() {
            this.debug("TRACE::_registerCallbacks()");

            pknyxui.model.EIBService.getInstance().registerListener(this._id, ["blind_position", "louvre_position"], this);
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");

            var louvreButton = new qx.ui.mobile.form.Button("Lames");
            louvreButton.addListener("tap", function(e) {
                this._louvrePopup.show();
            }, this);
            this._louvrePopup = this._createLouvrePopup(louvreButton);

            var blindButton = new qx.ui.mobile.form.Button("Volet");
            blindButton.addListener("tap", function(e) {
                this._blindPopup.show();
            }, this);
            this._blindPopup = this._createBlindPopup(blindButton);

            this.add(louvreButton);
            this.add(blindButton);
        },

        _createLouvrePopup : function(anchor) {
            if (this._louvrePopup) {
                return this._louvrePopup;
            }

            var increaseButton = new qx.ui.mobile.form.Button("F", "qx/icon/Oxygen/22/actions/go-down.png");
            increaseButton.addListener("tap", this._onIncreaseButtonTap, this);
            var decreaseButton = new qx.ui.mobile.form.Button("O", "qx/icon/Oxygen/22/actions/go-up.png");
            decreaseButton.addListener("tap", this._onDecreaseButtonTap, this);

            var buttonsWidget1 = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.HBox());
            buttonsWidget1.add(increaseButton, {"flex": 1});
            buttonsWidget1.add(decreaseButton, {"flex": 1});

            var closeButton = new qx.ui.mobile.form.Button("F");
            closeButton.addListener("tap", this._onCloseButtonTap, this);
            var pos2Button = new qx.ui.mobile.form.Button("2");
            pos2Button.addListener("tap", this._onPos2ButtonTap, this);
            var pos1Button = new qx.ui.mobile.form.Button("1");
            pos1Button.addListener("tap", this._onPos1ButtonTap, this);
            var openButton = new qx.ui.mobile.form.Button("O");
            openButton.addListener("tap", this._onOpenButtonTap, this);

            var buttonsWidget2 = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.HBox());
            buttonsWidget2.add(closeButton);
            buttonsWidget2.add(pos2Button);
            buttonsWidget2.add(pos1Button);
            buttonsWidget2.add(openButton);

            var buttonsWidget = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.VBox());
            buttonsWidget.add(buttonsWidget1);
            buttonsWidget.add(buttonsWidget2);

            var popup = new qx.ui.mobile.dialog.Popup(buttonsWidget, anchor);
            popup.setTitle("Lamelles");
            return popup;
        },

        _createBlindPopup : function(anchor) {
            if (this._blindPopup) {
                return this._blindPopup;
            }

            var downButton = new qx.ui.mobile.form.Button("F", "qx/icon/Oxygen/22/actions/go-down.png");
            downButton.addListener("tap", this._onDownButtonTap, this);
            var upButton = new qx.ui.mobile.form.Button("O", "qx/icon/Oxygen/22/actions/go-up.png");
            upButton.addListener("tap", this._onUpButtonTap, this);

            var buttonsWidget = new qx.ui.mobile.container.Composite(new qx.ui.mobile.layout.HBox());
            buttonsWidget.add(downButton);
            buttonsWidget.add(upButton);

            var popup = new qx.ui.mobile.dialog.Popup(buttonsWidget, anchor);
            popup.setTitle("Volet");
            return popup;
        },

        _onIncreaseButtonTap: function(e) {
            this.debug("TRACE::_onIncreaseButtonTap()");
            pknyxui.model.EIBService.getInstance().write(this._id, "louvre_step", "Increase");
            this._louvrePopup.hide();
        },

        _onDecreaseButtonTap: function(e) {
            this.debug("TRACE::_onDecreaseButtonTap()");
            pknyxui.model.EIBService.getInstance().write(this._id, "louvre_step", "Decrease");
            this._louvrePopup.hide();
        },

        _onCloseButtonTap: function(e) {
            this.debug("TRACE::_onCloseButtonTap()");

            pknyxui.model.EIBService.getInstance().write(this._id, "louvre_moveto", 100);
            this._louvrePopup.hide();
        },

        _onPos2ButtonTap: function(e) {
            this.debug("TRACE::_onPos2ButtonTap()");

            pknyxui.model.EIBService.getInstance().write(this._id, "louvre_moveto", 66);
            this._louvrePopup.hide();
        },

        _onPos1ButtonTap: function(e) {
            this.debug("TRACE::_onPos1ButtonTap()");

            pknyxui.model.EIBService.getInstance().write(this._id, "louvre_moveto", 33);
            this._louvrePopup.hide();
        },

        _onOpenButtonTap: function(e) {
            this.debug("TRACE::_onOpenButtonTap()");

            pknyxui.model.EIBService.getInstance().write(this._id, "louvre_moveto", 0);
            this._louvrePopup.hide();
        },

        _onDownButtonTap: function(e) {
            this.debug("TRACE::_onDownButtonTap()");

            pknyxui.model.EIBService.getInstance().write(this._id, "blind_updown", "Down");
            this._blindPopup.hide();
        },

        _onUpButtonTap: function(e) {
            this.debug("TRACE::_onUpButtonTap()");

            pknyxui.model.EIBService.getInstance().write(this._id, "blind_updown", "Up");
            this._blindPopup.hide();
        },

        update: function(id, knx, value) {
            this.debug("DEBUG::update(): id=", id, ", knx=", knx, ", value=", value);

            switch (knx) {
                case "blind_position":
                    break;

                case "louvre_position":
                    break;

                default:
                    break;
            }
        }
    }
});
