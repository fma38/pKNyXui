/*
 * A simple heating widget, providing a setpoint and a heater state feedback
 *
 * input objects:
 * - 'setpoint': object used by the widget to show the setpoint (DPT_Value_Temp 9.001)
 * - 'state': object used by the widget to show the heater state (DPT_Switch 1.001)
 *
 * output objects:
 * - 'setpoint': object driven by the widget when the setpoint changes (DPT_Value_Temp 9.001)
 *
 * TODO: use options for min/max/step
 */
qx.Class.define("pknyxui.widget.HeatingWidget",
{
    extend: pknyxui.widget.BaseWidget,

    properties:
    {
        setpoint: {
            init: 19.0,
            event: "changeSetpoint"
        }
    },

    members:
    {
        _setpointNumberFormat: null,
        _commandImage: null,

        _registerCallbacks: function() {
            this.debug("TRACE::_registerCallbacks()");

            // Internal callbacks
            this.addListener("changeSetpoint", this._onChangeSetpoint, this);

            // KNX listeners
            pknyxui.model.EIBService.getInstance().registerListener(this._id, ["setpoint", "state"], this);
            // TODO: move to parent class, and use property for listeners?
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");

            // Setpoint number
            this._setpointNumberFormat = new qx.util.format.NumberFormat().set({
                minimumFractionDigits: 1,
                maximumFractionDigits: 1,
                postfix: " °C"
            });

            // Minus button
            var minusButton = new qx.ui.mobile.form.Button("-");  //, "qx/icon/Oxygen/22/actions/go-down.png");
            minusButton.addListener("tap", this._onMinusButtonTap, this);
            this.add(minusButton);

            // Setpoint
            this._setpointLabelButton = new qx.ui.mobile.form.Button(this._setpointNumberFormat.format(this.getSetpoint()));
            this._setpointLabelButton.setActivatable(false);
//             this._setpointLabelButton.addCssClass("");
            this.add(this._setpointLabelButton);

            // Plus button
            var plusButton = new qx.ui.mobile.form.Button("+");  //, "qx/icon/Oxygen/22/actions/go-up.png");
            plusButton.addListener("tap", this._onPlusButtonTap, this);
            this.add(plusButton);

            // Spacer
            this.add(new qx.ui.mobile.form.Label(""));

            // Command
            this._commandImage = new qx.ui.mobile.basic.Image("qx/icon/Oxygen/32/actions/system-shutdown.png");
            this._commandImage.setActivatable(false);
            this._commandImage.setEnabled(false);
            this.add(this._commandImage);
        },

        _onChangeSetpoint: function() {
            this._setpointLabelButton.setLabel(this._setpointNumberFormat.format(this.getSetpoint()));
        },

        _onMinusButtonTap: function(e) {
            this.debug("TRACE::_onMinusButtonTap()");

            this.setSetpoint(this.getSetpoint() - 0.5);  // TODO: use option for step
            pknyxui.model.EIBService.getInstance().write(this._id, "setpoint", this.getSetpoint());
        },

        _onPlusButtonTap: function(e) {
            this.debug("TRACE::_onPlusButtonTap()");

            this.setSetpoint(this.getSetpoint() + 0.5);
            pknyxui.model.EIBService.getInstance().write(this._id, "setpoint", this.getSetpoint());
        },

        update: function(id, knx, value) {
            this.debug("update(): id=", id, ", knx=", knx, ", value=", value);

            switch (knx) {
                case "setpoint":
                    this.setSetpoint(Number(value));
                    break;

                case "state":
                    this._commandImage.setEnabled(Boolean(pknyxui.model.DPTService.getInstance().convert("1.001", value)));
                    break;

                default:
                    break;
            }
        }
    }
});
