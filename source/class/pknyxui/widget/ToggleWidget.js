/*
 * A simple toggle switch widget
 *
 * input objects:
 * - 'state': object used by the widget to show the switch state  (DPT_Switch 1.001)
 *
 * output objects:
 * - 'command': object driven by the widget when the switch state changes  (DPT_Switch 1.001)
 *
 */
qx.Class.define("pknyxui.widget.ToggleWidget",
{
    extend: pknyxui.widget.BaseWidget,

    members:
    {
        _handledDPT: null,
        _toggleButton: null,

        _init: function() {
            this.debug("TRACE::_init()");

//             this._handledDPT = new qx.data.Array();
//             this._handledDPT.append(["1.xxx", "1.001", "1.002", "1.003", "1.004", "1.005", "1.006", "1.007", "1.008",
//                                      "1.009", "1.010", "1.011", "1.012", "1.013", "1.014", "1.015", "1.016", "1.017",
//                                      "1.018", "1.019", "1.020", "1.021", "1.022", "1.023"]);

            // Check options
            if (!this._options.hasOwnProperty('label')) {
                this._options['label'] = ["I", "O"];
            }
//             if (!this._options.hasOwnProperty('dpt')) {
//                 this._options['dpt'] = "1.xxx";
//             }
//             else if (!this._handledDPT.contains(this._options['dpt'])) {
//                 this._options['dpt'] = "1.xxx";
//             }
//             this.debug("DEBUG::_init(): options=", this._options);
        },

        _registerCallbacks: function() {
            this.debug("TRACE::_registerCallbacks()");

            pknyxui.model.EIBService.getInstance().registerListener(this._id, ["state"], this);
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");

            this._toggleButton = new qx.ui.mobile.form.ToggleButton(false, this._options['label'][0], this._options['label'][1]);
            this._toggleButton.addListener("click", this._onToggleButtonClick, this);
            this._toggleButton.addListener("swipe", this._onToggleButtonClick, this);
            this.add(this._toggleButton);
        },

        _onToggleButtonClick: function(e) {
            var value = this._toggleButton.getValue();
            this.debug("DEBUG::_onToggleButtonClick(): value=", value);

//             pknyxui.model.EIBService.getInstance().write(this._id, "command", pknyxui.model.DPTService.getInstance().convert(this._options['dpt'], Number(value)));
            pknyxui.model.EIBService.getInstance().write(this._id, "command", Number(value));
        },

        update: function(id, knx, value) {
            this.debug("DEBUG::update(): id=", id, ", knx=", knx, ", value=", value);

//             this._toggleButton.setValue(Boolean(pknyxui.model.DPTService.getInstance().convert(this._options['dpt'], value)));
            this._toggleButton.setValue(Boolean(value));
        }
    }
});
