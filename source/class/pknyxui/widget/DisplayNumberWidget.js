/*
 * A simple display widget (floats only, at the moment)
 *
 * options:
 * - 'decimals'
 * - 'prefix'
 * - 'postfix'
 *
 * input objects:
 * - 'value': object used by the widget to show the value (DPT 9.xxx)
 *
 * output objects:
 *
 * TODO: give dpt instead of prefix, decimals (or allow both)
 * TODO: make a base display, and some specialized displays (string, int, float...)
 */
qx.Class.define("pknyxui.widget.DisplayNumberWidget",
{
    extend: pknyxui.widget.BaseWidget,

    members:{
        _numberFormat: null,

        _init: function() {
            this.debug("TRACE::_init()");

            // Check options
            if (!this._options.hasOwnProperty('decimals')) {
                this._options['decimals'] = 1;
            }
            if (!this._options.hasOwnProperty('prefix')) {
                this._options['prefix'] = "";
            }
            if (!this._options.hasOwnProperty('postfix')) {
                this._options['postfix'] = "";
            }
//             this.debug("DEBUG::_init(): options=", this._options);
        },

        _registerCallbacks: function() {
            this.debug("TRACE::_registerCallbacks()");

            pknyxui.model.EIBService.getInstance().registerListener(this._id, ["value"], this);
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");

            // Display number
            this._numberFormat = new qx.util.format.NumberFormat().set({
                minimumFractionDigits: this._options['decimals'],
                maximumFractionDigits: this._options['decimals'],
                prefix: this._options['prefix'],
                postfix: this._options['postfix']
            });

            // Display
            this._labelButton = new qx.ui.mobile.form.Button("");
            this._labelButton.setActivatable(false);
//             this._labelButton.addCssClass("");
            this.add(this._labelButton);
        },

        update: function(id, knx, value) {
            this.debug("update(): id=", id, ", knx=", knx, ", value=", value);
            this._labelButton.setValue(this._numberFormat.format(Number(value)));
        }
    }
});
