/*
 * A simple button widget
 *
 * options:
 * - 'label': label on the button
 * - 'value': value sent by the widget on buton tap  (DPT 1.xxx)
 *
 * input objects:
 *
 * output objects:
 * - 'command': object driven by the widget on buton tap  (DPT 1.xxx)
 *
 */
qx.Class.define("pknyxui.widget.ButtonWidget",
{
    extend: pknyxui.widget.BaseWidget,

    members:
    {
        _init: function() {
            this.debug("TRACE::_init()");

            // Check options
            if (!this._options.hasOwnProperty('value')) {
                this._options['value'] = 0;
            }
//             this.debug("DEBUG::_init(): options=", this._options);
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");

            if (this._options.hasOwnProperty('icon')) {
                var button = new qx.ui.mobile.form.Button(this._options['label'], "qx/icon/"+this._options['icon']);
            }
            else {
                var button = new qx.ui.mobile.form.Button(this._options['label']);
            }
            button.addListener("tap", this._onButtonTap, this);
            this.add(button);
        },

        _onButtonTap: function(e) {
            this.debug("TRACE::_onButtonTap()");
            pknyxui.model.EIBService.getInstance().write(this._id, "command", this._options['value']);
        }
    }
});
