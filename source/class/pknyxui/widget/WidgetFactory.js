qx.Class.define("pknyxui.widget.WidgetFactory",
{
    type : "singleton",

    extend: qx.core.Object,

    members:
    {
        create: function(id, type, options, desc) {
            switch (type) {
                case "button":
                    return new pknyxui.widget.ButtonWidget(id, options, desc);

                case "buttons":
                    return new pknyxui.widget.ButtonsWidget(id, options, desc);

                case "button2":
                    return new pknyxui.widget.Button2Widget(id, options, desc);

                case "toggle":
                    return new pknyxui.widget.ToggleWidget(id, options, desc);

                case "spin":
                    return new pknyxui.widget.SpinWidget(id, options, desc);

                case "sunblind":
                    return new pknyxui.widget.SunblindWidget(id, options, desc);

                case "heating":
                    return new pknyxui.widget.HeatingWidget(id, options, desc);

                case "displayNumber":
                    return new pknyxui.widget.DisplayNumberWidget(id, options, desc);

                case "displayMessage":
                    return new pknyxui.widget.DisplayMessageWidget(id, options, desc);

                case "slider":
                    return new pknyxui.widget.SliderWidget(id, options, desc);

//                 case "temperature":
//                     return new pknyxui.widget.TemperatureWidget(id, options, desc);

                default:
                    this.debug("ERROR::create(): unknown widget type", type);
                    return null;
            }
        }
    }
});
