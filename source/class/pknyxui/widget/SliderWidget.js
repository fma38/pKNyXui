/*
 * A slider widget
 *
 * options:
 * - 'minimum': minimum slider value
 * - 'maximum': maximum slider value
 * - 'step': step slider value
 *
 * input objects:
 * - 'value': object used by the widget to show the value (DPT 9.xxx)
 *
 * output objects:
 * - 'value': object driven by the widget on value change  (DPT x.xxx)
 *
 */
qx.Class.define("pknyxui.widget.SliderWidget",
{
    extend: pknyxui.widget.BaseWidget,

    members:
    {
        _slider: null,

        _init: function() {
            this.debug("TRACE::_init()");

            // Check options
            if (!this._options.hasOwnProperty('minimum')) {
                this._options['minimum'] = 0;
            }
            if (!this._options.hasOwnProperty('maximum')) {
                this._options['maximum'] = 100;
            }
            if (!this._options.hasOwnProperty('step')) {
                this._options['step'] = 1;
            }
//             this.debug("DEBUG::_init(): options=", this._options);
        },

        _registerCallbacks: function() {
            this.debug("TRACE::_registerCallbacks()");

            pknyxui.model.EIBService.getInstance().registerListener(this._id, ["value"], this);
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");

            this._slider = new qx.ui.mobile.form.Slider();
            this._slider.setDisplayValue("value");
            this._slider.addListener("trackend", this._onMouseUp, this);
            this.add(this._slider, {"flex": 1});
        },

        _onMouseUp: function(e) {
            var value = this._slider.getValue();
            this.debug("DEBUG::_onMouseUp(): value=", value);

            pknyxui.model.EIBService.getInstance().write(this._id, "value", value);
        },

        update: function(id, knx, value) {
            this.debug("DEBUG::update(): id=", id, ", knx=", knx, ", value=", value);

            this._slider.setValue(value);
        }
    }
});
