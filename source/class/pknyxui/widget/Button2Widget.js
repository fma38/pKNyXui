/*
 * A double buttons widget
 *
 * options:
 * - 'label': labels on the buttons (list)
 * - 'value': values sent by the widget on buton tap (list)
 *
 * input objects:
 *
 * output objects:
 * - 'command0': object driven by the widget on first buton tap
 * - 'command1': object driven by the widget on second buton tap
 *
 */
qx.Class.define("pknyxui.widget.Button2Widget",
{
    extend: pknyxui.widget.BaseWidget,

    members:
    {
        _button: null,

        _init: function() {
            this.debug("TRACE::_init()");

            // Check options
            if (!this._options.hasOwnProperty('label')) {
                this._options['label'] = ["O", "I"];
            }
            if (!this._options.hasOwnProperty('value')) {
                this._options['value'] = [0, 1];
            }
//             this.debug("DEBUG::_init(): options=", this._options);
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");

            if (this._options.hasOwnProperty('icon')) {
                var button1 = new qx.ui.mobile.form.Button(this._options['label'][0], "qx/icon/"+this._options['icon'][0]);
                var button2 = new qx.ui.mobile.form.Button(this._options['label'][1], "qx/icon/"+this._options['icon'][1]);
            }
            else {
                var button1 = new qx.ui.mobile.form.Button(this._options['label'][0]);
                var button2 = new qx.ui.mobile.form.Button(this._options['label'][1]);
            }

            button1.addListener("tap", this._onButton1Tap, this);
            button2.addListener("tap", this._onButton2Tap, this);

            this.add(button1);
            this.add(button2);

        },

        _onButton1Tap: function(e) {
            this.debug("TRACE::_onButton1Tap()");

            pknyxui.model.EIBService.getInstance().write(this._id, "command1", this._options['value'][0]);
        },

        _onButton2Tap: function(e) {
            this.debug("TRACE::_onButton2Tap()");

            pknyxui.model.EIBService.getInstance().write(this._id, "command2", this._options['value'][1]);
        }

    }
});
