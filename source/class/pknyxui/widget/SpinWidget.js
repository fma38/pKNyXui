/*
 * A simple spin button widget
 *
 * input objects:
 * - 'value': object used by the widget to show the switch state  (DPT_Switch 1.001)
 *
 * output objects:
 * - 'value': object driven by the widget when the switch state changes  (DPT_Switch 1.001)
 *
 */
qx.Class.define("pknyxui.widget.SpinWidget",
{
    extend: pknyxui.widget.BaseWidget,

    members:
    {
        _numberFormat: null,
        _numberField: null,

        _registerCallbacks: function() {
            this.debug("TRACE::_registerCallbacks()");

            pknyxui.model.EIBService.getInstance().registerListener(this._id, ["value"], this);
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");

            // Number
            this._numberFormat = new qx.util.format.NumberFormat().set({
                minimumFractionDigits: 1,  // TODO: use options
                maximumFractionDigits: 1,
                postfix: ""
            });

            var minusButton = new qx.ui.mobile.form.Button("-", "qx/icon/Oxygen/22/actions/go-down.png");
            minusButton.addListener("tap", this._onMinusButtonTap, this);
            this.add(minusButton);

            this._numberField = new qx.ui.mobile.form.NumberField(this._numberFormat.format(0.0));
            this._numberField.setMaxLength(10);
            this._numberField.addListener("changeValue", this._onNumberFieldChangeValue, this);
            this.add(this._numberField);

            var plusButton = new qx.ui.mobile.form.Button("-", "qx/icon/Oxygen/22/actions/go-up.png");
            plusButton.addListener("tap", this._onPlusButtonTap, this);
            this.add(plusButton);
        },

        _onNumberFieldChangeValue: function(e) {
            var value = e.getData();
            this.debug("DEBUG::_onNumberFieldChangeValue(): value=", value);

            pknyxui.model.EIBService.getInstance().write(this._id, "value", value);
        },

        _onMinusButtonTap: function(e) {
            this.debug("TRACE::_onMinusButtonTap()");

            this.setNumberFormat(this.getNumberFormat() - 0.5);
            pknyxui.model.EIBService.getInstance().write(this._id, "value", this.getNumberFormat());
        },

        _onPlusButtonTap: function(e) {
            this.debug("TRACE::_onPlusButtonTap()");

            this.setNumberFormat(this.getNumberFormat() + 0.5);
            pknyxui.model.EIBService.getInstance().write(this._id, "value", this.getNumberFormat());
        },

        update: function(id, knx, value) {
            this.debug("DEBUG::update(): id=", id, ", knx=", knx, ", value=", value);

            this._numberField.setValue(this._numberFormat.format(value));
        }
    }
});
