/**
 * @asset(qx/icon/*)
 */

qx.Class.define("pknyxui.widget.BaseWidget",
{
    extend: qx.ui.mobile.form.Group,

    implement : [pknyxui.model.EIBListener],

    properties:
    {
    },

    construct: function(id, options, label) {
        this.base(arguments);

        this._id = id;
        this._options = options

        this.setLayout(new qx.ui.mobile.layout.HBox());
        this.setShowBorder(false);

        this.add(new qx.ui.mobile.form.Label(label), {"flex": 1});

        this._init();
        this._registerCallbacks();
        this._buildUI();
    },

    members:
    {
        _init: function() {
            this.debug("TRACE::_init()");
        },

        _registerCallbacks: function() {
            this.debug("TRACE::_registerCallbacks()");
        },

        _buildUI: function() {
            this.debug("TRACE::_buildUI()");
        },

        update: function(id, knx, value) {
            this.debug("DEBUG::update(): id=", id, ", knx=", knx, ", value=", value);
        }
    }
});
