qx.Class.define("pknyxui.page.WidgetsPage",
{
    extend: qx.ui.mobile.page.NavigationPage,

    construct: function(title, widgets)
    {
        this.debug("construct(): title=", title);
        this.base(arguments);

        this._widgets = widgets;

        this.setTitle(title);
        this.setBackButtonText("Retour");
    },

    members:
    {
        _initialize: function() {
            this.debug("_initialize(): title=", this.getTitle());
            this.base(arguments);

            // create groups
            var groups = {};

            // Create/populate groups
            this._widgets.forEach(function(item, index, array) {

                // Create group if needed
                if (!groups.hasOwnProperty(item['group'])) {

                    // Create group
                    groups[item['group']] = new qx.ui.mobile.form.Group();
                    groups[item['group']].setLayout(new qx.ui.mobile.layout.VBox());
                    this.getContent().add(new qx.ui.mobile.form.Title(item['group']));
                    this.getContent().add(groups[item['group']]);
                    this.debug("_initialize(): created group", item['group']);
                }

                // Create widget and add to group
                var widget = pknyxui.widget.WidgetFactory.getInstance().create(item['id'], item['type'], item['options'], item['label']);
                if (widget != null) {
                    groups[item['group']].add(widget);
                }

                this.debug("_initialize(): added id=", item['id'], "/ widget=", item['type'], "to group", item['group']);
            }, this);

            // Force an eib refresh
            pknyxui.model.EIBService.getInstance().read();
        },

        _back: function() {
            qx.core.Init.getApplication().getRouting().back();
        }
    }
});
