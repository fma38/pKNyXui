/**
 * @asset(qx/icon/Oxygen/22/actions/system-shutdown.png)
 */
qx.Class.define("pknyxui.page.LinksPage",
{
    extend: qx.ui.mobile.page.NavigationPage,

    construct: function(links)
    {
        this.base(arguments);

        this._links = links;
    },

    members:
    {
        _useAltCss: false,

        _initialize: function() {
            this.debug("TRACE::_initialize()");
            this.base(arguments);

            // Create the list widget representing menu pages links
            var list = new qx.ui.mobile.list.List({
                configureItem: function(item, data, row) {
                    item.setTitle(data['title']);
                    item.setSubtitle(data['subtitle']);
                    item.setShowArrow(true);
//                     item.setImage("qx/icon/Oxygen/22/actions/system-shutdown.png");
                }
            });

            list.addListener("changeSelection", function(evt) {
                var model = list.getModel().toArray();
                var path = model[evt.getData()]['path'];
                qx.core.Init.getApplication().getRouting().executeGet("/"+path);
            }, this);

            list.setModel(this._links);

            if (this._useAltCss) {
                list.addCssClass("list-1");
            }

            this.getContent().add(list);
        },

        setAltCss: function() {
            this._useAltCss = true;
        }
    }
});
