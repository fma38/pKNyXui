qx.Class.define("pknyxui.page.BasePage",
{
    extend: qx.ui.mobile.page.NavigationPage,

    construct: function(config)
    {
        this.base(arguments);

        this._config = config;

        this.setNavigationBarToggleDuration(0.5);
        this.setShowBackButton(true);
        this.setBackButtonText("Retour");

        this._init();
    },

    members:
    {
        _init: function() {
        }
    }
});
